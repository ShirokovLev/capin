// Initialize and add the map
function initMap() {
    // The location of Uluru
    var uluru = {
      lat: 55.633983, 
      lng: 37.637194
    };

    // The map, centered at Uluru
    var map = new google.maps.Map(
      document.getElementById('map'), {
        zoom: 16,
        center: uluru,
      });
      
      image = '/assets/img/map-icon.svg';
      var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          icon: image
      });
  
    /* info-window */
  
    
  }
  
  
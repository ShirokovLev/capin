$(document).ready(function(){
    $(".js-range-slider").ionRangeSlider({
        skin: "big",
        type: "single",
        min: 0,
        max: 1000,
        from: 0,
        grid: false,
        hide_min_max: false,
        hide_from_to: true,
        postfix: '',
        onChange: function (data) {
            // Called every time handle position is changed
           
            
            $(data.slider).parents('.calculator-input').find('.calculator-input__input').val(data.from);
        },
    });
    

    // doc slider

    $('.documents-slider__rail').slick({
        dots: true,
        appendDots: '.documents-slider__slider-nav',
        prevArrow: '.documents-slider__prev',
        nextArrow: '.documents-slider__next',
        slidesToShow: 3,
        responsive: [
            {
              breakpoint: 990,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                arrows: false
              }
            },
        ]
    })

    // end doc slider

    // language switcher

    $('.language-switcher').on('click', function(){
        $(this).toggleClass('language-switcher_active');
    });

    // end language switcher

    

    // hamburger

    $('.hamburger').on('click', function(){
        $('.header-xs').toggleClass('header-xs_active');
        $('body').toggleClass('body-ofh');
        $(this).toggleClass('is-active');
    });

    // end hamburger

    // investor slider

    $('.tariff-slider__rail').slick({
        dots: true,
        appendDots: '.tariff-slider__slider-nav',
        prevArrow: '.tariff-slider__prev',
        nextArrow: '.tariff-slider__next',
        adaptiveHeight: true,
        slidesToShow: 1,
        responsive: [
            {
              breakpoint: 990,
              settings: {
                infinite: true,
                arrows: false
              }
            },
        ]
    });

    // end investor slider

    // file input

    $('#input-file').on('change',function(){
        
        var files = this.files

        for (var i = 0, numFiles = files.length; i < numFiles; i++) {
            var file = files[i];
            

            if (!file.type.startsWith('image/')){ continue }

            var img = document.createElement("img"), preview = $('.input-file__files');
            img.classList.add("obj");
            img.file = file;

            preview.append(img); // Предполагается, что "preview" это div, в котором будет отображаться содержимое.

            var reader = new FileReader();
            reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
            reader.readAsDataURL(file);
        }
    });

    // end file input

    $('.feedback-item__fulltext-trigger').on('click', function(e){
        e.preventDefault();
        $(this).parents('.feedback-item').find('.feedback-item__text').toggleClass('feedback-item__text_active');
        if($(this).find('span').html() === 'Читать полностью'){
            $(this).find('span').html('Скрыть')
        } else {
            $(this).find('span').html('Читать полностью')
        }
    });

    $('.input-block__password-switcher').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('input-block__password-trigger_active');
        if( $(this).hasClass('input-block__password-trigger_active') ){
            $(this).parent().find('.input-block__input').attr('type', 'text');
        } else {
            $(this).parent().find('.input-block__input').attr('type', 'password');
        }
        
    });

    $('.tariff-slider__triple-rail').slick({
        dots: true,
        appendDots: '.tariff-slider__slider-nav',
        prevArrow: '.tariff-slider__prev',
        nextArrow: '.tariff-slider__next',
        adaptiveHeight: true,
        slidesToShow: 3,
        responsive: [
            {
              breakpoint: 990,
              settings: {
                infinite: true,
                arrows: false,
                slidesToShow: 1,
              }
            },
        ]
    });

    

    
});